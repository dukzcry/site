---
title: Глупые IT названия
published: 11.05.2023
tags: vintage
---

Бриллиантовый монстр и пляжная черепашка поедают галету с помидорами 🥴

- Материнская плата: ZIDA Tomato Board
- Звуковая карта: Turtle Beach
- Видеокарта: Diamond Monster 3D

<details> 
  <summary>Процессор ИСКРА компании Солнечные микросистемы</summary>
   SPARC (Scalable Processor ARChitecture — масштабируемая архитектура процессора) — архитектура RISC-микропроцессоров, первоначально разработанная в 1985 году компанией Sun Microsystems
</details>
<details> 
  <summary>FM радио "Города"</summary>
   FM Towns (иногда используются обозначения FM-Towns, FM TOWNS, FM-TOWNS) — японский бытовой компьютер. Создан фирмой Fujitsu, продавался с февраля 1989 по лето 1997 года. Название «FM Towns» произошло от слова «Townes», кодового обозначения системы, которое она имела в процессе разработки. Такое название было выбрано в честь Чарлза Харда Таунса, лауреата Нобелевской премии 1964 года в области физики, следуя используемой в то время фирмой Fujitsu практике кодового обозначения продуктов, относящихся к PC, именами нобелевских лауреатов
</details>
<details> 
  <summary>НОВОСТИ от Солнечных ребят</summary>
   The Sony NEWS ("Network Engineering Workstation", later "NetWorkStation") is a series of Unix workstations sold during the late 1980s and 1990s
</details>
<details> 
  <summary>Профессиональный Ирис "Башни близнецы"</summary>
   The first of these Silicon Graphics systems was the 4D/60 'Professional IRIS', sporting a MIPS R2300 clocked at 8 MHz in a unique 'twin-tower' case, with the cardcage being within the larger tower on the left, and the power supply and drives being kept within the smaller tower on the right
</details>
<details> 
  <summary>Сетевая карта "Детка"</summary>
   The Sun386i workstation motherboard includes the CPU, 80387 FPU, 82380 timer/DMA/interrupt controller and a custom Ethernet IC called BABE ("Bus Adapter Between Ethernet")
</details>