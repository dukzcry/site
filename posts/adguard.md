---
title: Настройки AdGuard Home
published: 28.02.2025
tags: dns, dhcp, adblock
---
# Настройки
## Настройки DNS
### Upstream DNS-серверы
```
# cloud9 с tls троттлит при большой нагрузке
8.8.8.8
8.8.4.4
# внешние dns сервера для определённых доменов
[/домен1/домен2/]1.2.3.4 5.6.7.8
# использовать глобальные dns для этих поддоменов
[/исключение1.домен1/исключение2.домен2/]#
# для arr стека
[/themoviedb.org/]9.9.9.9
[/tmdb.org/]9.9.9.9
```
## Настройки клиентов
Имя клиента `yaos`  
Идентификатор `a9:cf:1d:65:79:9a`

# Фильтры
## Пользовательские правила фильтрации
```
# для arr стека
||api.radarr.video^$$$$dnsrewrite=1.2.3.5,client=~1.2.3.4
||radarr.servarr.com^$$$$dnsrewrite=1.2.3.5,client=~1.2.3.4
# правила для yaos
||quasar.yandex.net^$$$$client=yaos
||appmetrica.yandex.net^$$$$client=yaos
||rpc.alice.yandex.net^$$$$client=yaos
```